<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $table = 'data';
    public static $rules = [
    'judul' 		=> 'min:3|required|unique:data',
    'keterangan'    =>'required',
    ];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];
	protected $hidden = ['id'];
	public $timestamps = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	public function danger()
	{
		return encrypt($this->id);
	}
}
