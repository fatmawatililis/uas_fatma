<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Data;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Validator;
use Illuminate\Support\Facades\Input;

class DataController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getIndex()
    {

    	$data = Data::query()->orderBy('created_at','desc')->get();
    	return Datatables::of( $data )
                ->addColumn( 'action', function ( $data )
                    {
                        if (Auth::user()->hasRole('user')) {
                             $data = '';
                        }else{
                            $data = '<a href="'.route("data.edit",$data->danger()).'" class="btn btn-warning btn-xs" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a> <form action="'.route("data.destroy",$data->danger()).'" method="POST"><input type="hidden" name="_method" value="DELETE">'.csrf_field().'<button name="submit" class="btn btn-danger btn-xs" ><i class="fa fa-times" aria-hidden="true"></i> Delete</button></form>';
                        }
                       

                        return $data;
                    }
                )
                 ->editColumn('created_at', function(Data $data) {
                    return $data->created_at->diffForHumans();
                })
                ->addIndexColumn()
                ->make( TRUE );
    }

    public function create()
    {
        $data['title']  = "Buat Data";
        $data['users']   = Auth::user();
        return view('data.create')->with($data);
    }

    public function store(Request $request)
    {

    	$valid = Data::$rules;
        $validationCertificate  = Validator::make($request->all(), $valid);
        if ($validationCertificate->fails()) {
        	$this->throwValidationException($request, $validationCertificate);
        }

        $data =  Data::create([
        	'judul'			=> $request->judul,
        	'keterangan'	=> $request->keterangan
        	]);

        return redirect(route('home'))->with('status', 'Data Baru Berhasil Dibuat');
    }

    public function destroy($datum){

    	// delete
        $datum = decrypt($datum);
        Data::destroy($datum);

        return redirect(route('home'))->with('status', 'Data Berhasil Dihapus');
    }

    public function edit($datum){

        $datum = decrypt($datum);
    	$data['title']  = "Edit Data";
    	$data['data']	= Data::find($datum);
        $data['users']   = Auth::user();
        return view('data.edit')->with($data);
    }

    public function update(Request $request,$datum)
    {
        $datum = decrypt($datum);
    	$data = Data::$rules;
    	$data['judul'] = $data['judul'] . ',judul,' . $datum;
        $validationCertificate  = Validator::make($request->all(), $data);
        if ($validationCertificate->fails()) {
        	$this->throwValidationException($request, $validationCertificate);
        }

        $datas = Data::find($datum);
       	$datas->judul = $request->judul;
	    $datas->keterangan = $request->keterangan;
	    $datas->update();
        
        return redirect(route('home'))->with('status', 'Data Berhasil Diperbarui');
    }
}
