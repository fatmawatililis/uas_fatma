<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use Validator;
use Hash;
class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title']  = "Profile";
        $data['users']   = Auth::user();
        return view('profile.index')->with($data);
    }

    public function edit()
    {
        $data['title']  = "Edit Profile";
        $data['users']   = Auth::user();
        return view('profile.edit')->with($data);
    }

    public function change()
    {
        $data['title']  = "Ganti Password";
        return view('profile.password')->with($data);
    }

    public function credential_users(array $data)
    {
      $messages = [
        'current-password.required' => 'Please enter current password',
        'password.required' => 'Please enter password',
      ];

      $validator = Validator::make($data, [
        'current-password' => 'required',
        'password' => 'required|same:password',
        'password_confirmation' => 'required|same:password',     
      ], $messages);

      return $validator;
    }  

    public function post(Request $request)
    {
        if(Auth::Check())
              {
                $request_data = $request->All();
                $validator = $this->credential_users($request_data);
                if($validator->fails())
                {
                  $errors = $validator->getMessageBag()->toArray(); 
                  $out = '<ul>';
                  foreach($errors as $key => $value) {
                          $out.= '<li>'.$value[0].'</li>';
                  }
                  $out .= '</ul>';
                  return redirect()->back()->with('status', $out); 
                }
                else
                {  
                  $current_password = Auth::User()->password;           
                  if(Hash::check($request_data['current-password'], $current_password))
                  {           
                    $user_id = Auth::User()->id;                       
                    $obj_user = User::find($user_id);
                    $obj_user->password = Hash::make($request_data['password']);;
                    $obj_user->save(); 
                    return redirect(route('profile'))->with('status', 'Password Berhasil Diperbarui');
                  }
                  else
                  {           
                    return redirect()->back()->with('status', 'Please enter correct current password');   
                  }
                }        
              }
              else
              {
                return redirect()->to('/');
              }  
    }

    public function store(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user->name = $request->name;
        $user->alamat = $request->alamat;
        $user->nomer = $request->nomer;
        $user->save();
        return redirect(route('profile'))->with('status', 'Profile Berhasil Diperbarui');
    }
}
