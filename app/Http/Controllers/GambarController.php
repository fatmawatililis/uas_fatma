<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GambarController extends Controller
{
    public function index()
    {
    	return view('gambar.index')->with(['data'=>"hasil"]);
    }

    public function store(Request $request)
    {
    	$gambar = $request->file('gambar');
    	$gambar_encode = base64_encode(file_get_contents($gambar));

    	$data = [
    		'data' => $gambar_encode
    	];

    	return response()->json($data);
    }
}
