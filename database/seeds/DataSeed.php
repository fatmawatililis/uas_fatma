<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\Role;
use App\User;

class DataSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for ($i=0; $i < 50; $i++) 
    	{ 
    		$faker = Faker::create();
    		DB::table('data')->insert([
	            'judul' => $faker->name,
	            'keterangan' => $faker->text(50),
	            'created_at' => $faker->dateTime(),
	            'updated_at' => $faker->dateTime(),
        	]);
    	}

        $roles = [
            [
                'name' => 'admin',
                'display_name' => 'Administration',
                'description' => 'Only one and only admin',
            ],
            [
                'name' => 'user',
                'display_name' => 'User',
                'description' => 'User',
            ]
        ];
        foreach ($roles as $key => $value) {
                    Role::create($value);
                }

        $users = [
            [
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'verified' => 1,
                'password' => bcrypt('admin123'),
                'alamat' => 'alamat',
                'nomer' => '089632827136',
            ],
        ];
        foreach ($users as $key => $value) {
            $user=User::create($value);
            $user->attachRole(1);
        }
    }
}
