@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$title or "Heading"}}</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-danger">
                            {!! session('status') !!}
                        </div>
                    @endif

                    <form id="form-change-password" role="form" method="POST" action="{{ route('password.post') }}" novalidate class="form-horizontal">
                      <div class="col-md-9">             
                        <label for="current-password" class="col-sm-4 control-label">Password Aktif</label>
                        <div class="col-sm-8">
                          <div class="form-group">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                            <input type="password" class="form-control" id="current-password" name="current-password" placeholder="Password">
                          </div>
                        </div>
                        <label for="password" class="col-sm-4 control-label">Password Baru</label>
                        <div class="col-sm-8">
                          <div class="form-group">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                          </div>
                        </div>
                        <label for="password_confirmation" class="col-sm-4 control-label">Ulangi Password Baru</label>
                        <div class="col-sm-8">
                          <div class="form-group">
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Re-enter Password">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                          <button type="submit" class="btn btn-danger"><i class="fa fa-key" aria-hidden="true"></i> Submit</button>
                          <a class="btn btn-warning" href="{{url()->previous()}}"><i class="fa fa-undo" aria-hidden="true"></i> Kembali</a>
                        </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
