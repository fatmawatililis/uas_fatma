@extends('layouts.app')


@section('head')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$title or "Heading"}}</div>
                <div class="panel-body">
                @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                @endif
                  <center><table class="table" style="padding:5px;">
                        <tbody><tr>
                            <td colspan="3">
                                <h3><font face="Arial">{{$users->name}}</font></h3>
                                <p>{{$users->nomer}}</p>
                            </td>
                            <td rowspan="7">
                                <img src="{{url('/images/'.$users->foto)}}" width="200">
                            </td>
                        </tr>
                        <tr>
                            <td>Alamat</td><td>:</td><td>{{$users->alamat}}</td>
                        </tr>
                        <tr>
                            <td>Email</td><td>:</td><td><a href="mail:{{$users->email}}">{{$users->email}}</a></td>
                        </tr>
                    </tbody></table></center>
                </div>
                <div class="panel-body">
                <a href="{{route('profile.edit')}}" class="btn btn-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
 Edit</a>
                <a href="{{route('password.index')}}" class="btn btn-danger"><i class="fa fa-key" aria-hidden="true"></i> Ganti Password</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

@endsection
