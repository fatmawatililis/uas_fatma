<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Tugas UAS Pemrograman Website</title>

  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">

  <!-- font -->
  <link href='/css/font-awesome.min.css' rel='stylesheet' type='text/css'/> 

</head>
<body>
<header>
   <!-- navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ url('/') }}">Tugas UAS Pemrograman Website</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
        @if (Auth::check())
            <li><a href="{{ route('home') }}"><strong> Data</strong></a></li>
            <li><a href="{{ route('gambar.index') }}"><strong> Konversi Gambar</strong></a></li>
            <li><a href="{{ url('/home') }}"><strong><i class="fa fa-home"></i> Home</strong></a></li>
        @else
            <li><a href="{{ url('/login') }}"><strong><i class="fa fa-sign-in"></i> Login</strong></a></li>
            <li><a href="{{ url('/register') }}"><strong><i class="fa fa-user-circle"></i> Register</strong></a></li>
        @endif
        </ul>
        </div>
      </div>
    </nav>
</header>

<section class="section satu" id="dua">
                      <div class="container">
    <div class="content-left-wrap col-md-12">
      <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
          <div class="panel panel-default">
              <div class="panel-heading">
                <h1>Hallo</h1>
              </div>
              <div class="panel-body">
                <pre>
Requirements :
- PHP > 7.*
- Database MYSQL/SQLITE/POSTGRESQL
- SMTP SERVER ( Untuk Konfirmasi Email Registrasi )

#install Laravel

Install laravel dengan composer package manager. jalankan perintah

`composer update`

#Konfigurasi .env

Copykan file .env.example dengan nama .env, atau jalankan perintah :

`php artisan key:generate`

Perintah tersebut akan otomatis membuat file .env beserta generate Application Keynya isi sesuai konfigurasi yang diinginkan. yang terpenting pastikan ubah bagian dibawah ini :

`CACHE_DRIVER=file`

menjadi

`CACHE_DRIVER=array`

#Melakukan Migrasi Database

Setelah konfigurasi di file .env sudah benar, lakukan migrasi database dengan perintah

`php artisan migrate`

Perintah tersebut akan membuat tabel tabel secara otomatis.

#Membuat Data Dummy

Setelah berhasil melakukan migrasi database, jalankan perintah dibawah ini :

`php artisan db:seed --class=DataSeed`

Perintah tersebut akan membuat data dummy, hak akses 'admin' dan 'user', juga user Admin default `admin@gmail.com` dengan password `admin123`

#NB

- Registrasi harus konfirmasi melalui email aktif
- User bisa diaktivasi manual di database dengan merubah value `verified` menjadi - Tugas 3 (enkripsi url) ada di list table data, di menu edit (HANYA ADMIN SAJA YANG BISA AKSES)
- Tugas 4 di menu konversi gambar


Regrads

Fatma ^_^


                </pre>
              </div>
          </div>
        </main><!-- #main -->
      </div><!-- #primary -->
    </div><!-- .content-left-wrap -->
  </div><!-- .container -->
</section> 
<!-- section 3 -->
  <script src="/js/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="/js/jquery.min.js"><\/script>')</script>
  <script src="/js/bootstrap.min.js"></script>
  <script type='text/javascript'>

    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function(){

            this.classList.toggle("active");

            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        }
    }

  </script>
</body>
</html>