@extends('layouts.app')


@section('head')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
                     @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
            <div class="panel panel-default">
                <div class="panel-heading">Data</div>
                <div class="panel-body">
                <a href="{{route('data.create')}}" class="btn btn-primary" style="float: right"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a>
                </div>
                <div class="panel-body">
                    <div class="box-body table-responsive">
                      <table id="data" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Judul</th>
                                <th>Keterangan</th>
                                <th>Dibuat</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Judul</th>
                                <th>Keterangan</th>
                                <th>Dibuat</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script>
$(function() {

    $('#data').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('data.getIndex') !!}',
        columns: [
            { data: 'DT_Row_Index', name: 'DT_Row_Index',orderable: false, searchable: false },
            { data: 'judul', name: 'judul' },
            { data: 'keterangan', name: 'keterangan' },
            { data: 'created_at', name: 'created_at',orderable: false, searchable: false },
            { data: 'action', name: 'action',orderable: false, searchable: false }

        ],
    });
});
</script>

@endsection
