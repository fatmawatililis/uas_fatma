@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Konversi Gambar ke base64</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" id="gambar" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="gambar" class="col-md-4 control-label">Masukkan Gambar</label>

                            <div class="col-md-6">
                                <input id="gambar" type="file" class="form-control" name="gambar" value="" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary"> Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Hasil</div>
                <div class="panel-body">
                    <textarea class="form-control" id="hasil" rows="20" ></textarea>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script type="text/javascript">
    $(document).on('submit', '#gambar', function(e){
          e.preventDefault();

          var form_data = new FormData($('#gambar')[0]);
          $.ajax({
              type:'POST',
              url:'{{ route('gambar.store') }}',
              processData: false,
              contentType: false,
              async: false,
              cache: false,
              data : form_data,
              success: function(response){
                $('#hasil').val(response.data);
              }
          });
        });
</script>
@endsection
