<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// public

Route::get('/', function () {
    return view('welcome');
});

// user

Auth::routes();
Route::get('register/verify/{token}', 'Auth\RegisterController@verify'); 

Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('/profile',['as' => 'profile', 'uses' => 'ProfileController@index']);
Route::get('/profile/edit',['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
Route::put('/profile/store',['as' => 'profile.store', 'uses' => 'ProfileController@store']);
Route::put('/profile/store',['as' => 'profile.store', 'uses' => 'ProfileController@store']);

Route::get('/password',['as' => 'password.index', 'uses' => 'ProfileController@change']);
Route::post('/password',['as' => 'password.post', 'uses' => 'ProfileController@post']);


Route::resource('data', 'DataController');
Route::get('/getIndex',['as' => 'data.getIndex', 'uses' => 'DataController@getIndex']);

// Gambar to base64
Route::resource('gambar', 'GambarController');
