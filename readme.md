#Requirements

- PHP > 7.*
- Database MYSQL/SQLITE/POSTGRESQL
- SMTP SERVER ( Untuk Konfirmasi Email Registrasi )

#install Laravel

Install laravel dengan composer package manager. jalankan perintah

`composer update`

#Konfigurasi .env

Copykan file .env.example dengan nama .env, atau jalankan perintah :

`php artisan key:generate`

Perintah tersebut akan otomatis membuat file .env beserta generate Application Keynya isi sesuai konfigurasi yang diinginkan. yang terpenting pastikan ubah bagian dibawah ini :

`CACHE_DRIVER=file`

menjadi

`CACHE_DRIVER=array`

#Melakukan Migrasi Database

Setelah konfigurasi di file .env sudah benar, lakukan migrasi database dengan perintah

`php artisan migrate`

Perintah tersebut akan membuat tabel tabel secara otomatis.

#Membuat Data Dummy

Setelah berhasil melakukan migrasi database, jalankan perintah dibawah ini :

`php artisan db:seed --class=DataSeed`

Perintah tersebut akan membuat data dummy, hak akses 'admin' dan 'user', juga user Admin default `admin@gmail.com` dengan password `admin123`

#NB

- Registrasi harus konfirmasi melalui email aktif
- User bisa diaktivasi manual di database dengan merubah value `verified` menjadi 1
- Tugas 3 (enkripsi url) ada di list table data, di menu edit (HANYA ADMIN SAJA YANG BISA AKSES)
- Tugas 4 di menu konversi gambar


Regrads

Fatma ^_^